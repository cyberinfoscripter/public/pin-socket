var app = require("express")();
var http = require("http").Server(app);
var io = require("socket.io")(http);

var users = {};

io.on("connection", function(socket) {
  let connectedUser = JSON.parse(socket.handshake.query.user);
  if (Object.keys(users).indexOf(connectedUser.uid) === -1) {
    console.log("new user connected");
  }

  users[connectedUser.uid] = connectedUser;

  socket.on("GET_ONLINE_USERS", (data, callBackFn) => {
    callBackFn(users);
  });
});

http.listen(3005, function() {
  console.log("listening on *:3005");
});
